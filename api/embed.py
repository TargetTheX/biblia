import discord
from api import channel as channel_list

"""
arts ---------------------- rose        #f54b9a
biographie ---------------- violet      #d751ff
économie ------------------ jaune       #e6ca18
environnement ------------- vert clair  #4dd84c
géopolitique -------------- mauve       #df4fd0
histoire ------------------ bleue foncé #7c6bff
informatique -------------- vert foncé  #31ac30
philosophie --------------- turquoise   #42aae4
santé --------------------- rouge       #e74040
sciences-physiques -------- marron      #e07039
sciences-sociales --------- beige       #f3dc72
société ------------------- menthe      #1de0aa
spiritualité -------------- orange      #ff9617
roman --------------------- bleue       #5388ff
autre ---------------------- gris       #a7a7a7

'🏯', '👴', '💰', '🌍', '🔮', '📜', '💻', '💭', '🐍', '🔬', '🔎', '🏠', '🌀', '📘', '❔'
"""

category_color = {
    '🏯' : '0xf54b9a',
    '👴' : '0xd751ff',
    '💰' : '0xe6ca18',
    '🌍' : '0x4dd84c',
    '🔮' : '0xdf4fd0',
    '📜' : '0x7c6bff',
    '💻' : '0x31ac30',
    '💭' : '0x42aae4',
    '🐍' : '0xe74040',
    '🔬' : '0xe07039',
    '🎓' : '0xf3dc72',
    '🏠' : '0x1de0aa',
    '🌀' : '0xff9617',
    '📘' : '0x5388ff',
    '❔' : '0xa7a7a7'
    }





def create_embed(book):

    """
    céé un embed pour un fiche de lecture

    :param book: dictionnaire de la fiche de lecture
    :return: objet embed
    """

    embed = discord.Embed()

    """if mode_edit:
        embed.title = ""
        un = 1
        for i in book["authors"]:
            if un == 1:
                embed.title = embed.title + i
            else:
                embed.title = embed.title + " / " + i

    else:"""

    # title related stuff
    # if book["title"]:
    # if book["date"]:
    embed.title = ""
    if book["authors"]:
        embed.title = embed.title + book["authors"]





    if book["title"]:
        embed.title = embed.title + "\n\n« " + book["title"] + " » "
    if book["date"]:

        embed.title = embed.title + " (" + str(book["date"]) + ")"




    # description related stuff
    embed.description = "-"

    if book["description"]:
        embed.description = embed.description + book["description"] + "\n\n\n"

    if book["categories"]:
        embed.description = embed.description + "["


        lib_icon = book["categories"][0]

        lib_idx = channel_list.list_emojis.index(lib_icon)
        lib_id = channel_list.list_channels[lib_idx].id
        embed.description = embed.description + f"<#{lib_id}>"

        """for i in range (1, len(book["categories"])):
            embed.description = embed.description + " ; " + book["categories"][i]"""

        embed.description = embed.description + "]"

        embed.color = int(category_color[book["categories"][0][0]], 16)
        # embed.color = 0x992d22

    if book["recommended_by"]:
        embed.description = embed.description + "\n\n🎬 Cité lors d'une interview par : **" + book["recommended_by"] + "**"

    # image stuff
    if book["img_cover"]:
        embed.set_image(url=book["img_cover"])

    return embed



def embed_start(compteur):
    """
    créé un embed avec les commande pour interagir avec le bot

    :param compteur: dictionnaire des statistique du bot
    :return: objet embed
    """
    embed = discord.Embed()
    embed.title = "Biblia"
    embed.description=f"« *Que voulez-vous faire ?* »\n\n\t📘 - **Ajouter** un ouvrage\n\t📝 - **Modifier** un ouvrage\n\t🔍 - **Rechercher** un ouvrage" \
                      f"\n\n\t    ***statistique***" \
                      f"\nouvrage : {compteur['enregistre']}" \
                      f"\nrecherche : {compteur['recherche']}" \
                      f"\nlecture : {compteur['lecture']}" \
                      f"\ncoup de coeur : {compteur['like']}"
    embed.set_thumbnail(url="https://cdn.discordapp.com/icons/753277958881280210/6312238a02fa8c4f70d88acef6cca208.webp?size=128")
    return embed

def embed_search(book):

    """
    créé un petit embed pour afficher un resultat d'une recherche

    :param book: dictionnaire de la fiche du livre
    :return: objet embed
    """

    print('embed search')
    print(book)
    embed = discord.Embed()
    embed.title = book["title"]
    try:
        if book["url"]:
            embed.url = book["url"]
    except:
        print("probleme url embed recherche")
    embed.description = f"{book['date']} - {book['authors']} \n {book['categories'][0]}"
    embed.color=int(category_color[book["categories"][0]], 16)

    return embed

