
from api.embed import embed_search
from api import dispatch,database
from api import channel as chan_list

#class -------------------------




"""
affiche le message de la session en fonction du step


:param session: instance de la session
:param bot: instance du bot
:return:
"""
async def session_print(session, bot):

    channel = bot.get_channel(session.channel_id)

    if session.step == 10:
        await channel.send("fermeture de la session")
        return

    if session.step == 1:
        step1_mess = await channel.send(session.message.search(session))
        step1_emoji = session.message.search(session, emoji=True)
        for emoji in step1_emoji:
            await step1_mess.add_reaction(emoji)
        return


    message = await channel.send(session.message.search(session))
    emojis =  session.message.search(session,emoji=True)

    if session.step == 3:
        mess_chanlist = "\n"
        for chan in chan_list.list_channels:
            mess_chanlist += str(chan) + "\n"
        await channel.send(mess_chanlist)
        message = await channel.send("**selectionner les categories dans les emojis**")
        for chan in chan_list.list_emojis:
            await message.add_reaction(chan)
        message = await channel.send('**commande**\n✅ valider la sélection\n❌ retour au menu')
        for emoji in emojis:
            await message.add_reaction(emoji)
    else:
        for emoji in emojis:
            await message.add_reaction(emoji)
    return



"""
gere l'entrer d'un message dans la session recherche

:param session: instance de la session
:param bot: instance du bot
:param message: instance du message
:return:
"""
async def session_input_message(session,bot,message):


    if session.step == 2: # mots clef
        if len(message.content) > 30:
            return

        session.fiche['mot_clef'] = message.content
        session.step = 1
        await session_print(session, bot)

    elif session.step == 4: #siecle
        try:
            session.fiche["siecle"] = int(message.content)

        except:
            await message.chanel.send(f"erreur: {message.content} n'est pas un chiffre entier")

        finally:
            session.step = 1
            await session_print(session,bot)



"""
gere l'entrer d'une reaction dans la session recherche

:param session: instance de la session
:param bot: instance du bot
:param reaction: instance de la reaction
:return:
"""
async def session_input_reaction(session,bot,reaction):
    if str(reaction.emoji) == "🔎":
        print('recherche activer')
        await search_and_close(session,bot)
        return

    elif str(reaction.emoji) == "❌":
        session.step = 1
        await session_print(session, bot)
        return

    elif str(reaction.emoji) == "⭕":
        session.step = 10
        await session_print(session, bot)
        await dispatch.close_session(session)
        return

    if session.step == 1: # control
        if str(reaction.emoji) == "🔑": # mot clef
            session.step = 2
            await session_print(session,bot)

        elif str(reaction.emoji) == "📂":  # categorie
            session.step = 3
            await session_print(session, bot)

        elif str(reaction.emoji) == "📅": # siecle
            session.step = 4
            await session_print(session, bot)

        elif str(reaction.emoji) == "🎬": # proposer part thinkerview
            session.step = 5
            await session_print(session, bot)

        elif str(reaction.emoji) == "📖": # readlist
            session.step = 6
            await session_print(session, bot)

        elif str(reaction.emoji) == "❤️": # coup de coeur
            session.step = 7
            await session_print(session, bot)



    elif session.step == 3: #categorie
        if str(reaction.emoji) == "✅":
            session.step = 1
            await session_print(session,bot)
            return

        session.fiche['categories'].append(str(reaction.emoji))
        return



    elif session.step == 5: # rocommended by
        if str(reaction.emoji) == "✅":
            session.fiche['tkw'] = True
            session.step = 1
            await session_print(session,bot)
            return

        if str(reaction.emoji) == "⏹":
            session.fiche['tkw'] = False
            session.step = 1
            await session_print(session, bot)
            return

    elif session.step == 6: # readlist
        if str(reaction.emoji) == "✅":
            session.fiche['readlist'] = True
            session.step = 1
            await session_print(session,bot)
            return

        if str(reaction.emoji) == "⏹":
            session.fiche['readlist'] = False
            session.step = 1
            await session_print(session, bot)
            return

    elif session.step == 7: # coup de coeur
        if str(reaction.emoji) == "✅":
            session.fiche['coupdecoeur'] = True
            session.step = 1
            await session_print(session, bot)
            return

        if str(reaction.emoji) == "⏹":
            session.fiche['coupdecoeur'] = False
            session.step = 1
            await session_print(session, bot)
            return



"""
lance la recherche, affiche les resultat et ferme la session

:param session: instance de la session
:param bot: instance du bot
:return:
"""
async def search_and_close(session,bot):

    channel = bot.get_channel(session.channel_id)
    list_livre = await database.recherche_dans_la_bibliotheque(session.fiche)
    if list_livre == []:
        await channel.send("aucun livre de la bibliotheque ne corespond a votre recherche")

    for livre in list_livre :
        await channel.send(embed=embed_search(livre))
    print("la recherche a ete effectuer")
    await dispatch.close_session(session)







